#!/bin/bash

DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)

pushd ${DIR}/.rsa
  encrypt="$(cat id_rsa_encrypt)"
popd

git_url="https://github.com/jpmorin/config-server-configs.git"

config="$(jq -n '{
  "count": 1,
  "git": {
    "searchPaths": "dev",
    "label": "master",
    "uri": "'"${git_url}"'",
    "periodic":"true"
  },
  "encrypt": {
    "key": "'"${encrypt}"'"
  }
}
')"

cf create-service p-config-server trial config-server -c "$(echo ${config} | jq -c .)"
# cf update-service config-server -c "$(echo ${config} | jq -c .)"
