using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Steeltoe.Common.Hosting;
using Steeltoe.Extensions.Configuration.CloudFoundry;
using Steeltoe.Extensions.Configuration.ConfigServer;
using Steeltoe.Extensions.Logging.DynamicLogger;

namespace Formation.SpringCloud.Service
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                
                // Écoute sur le port défini par la variable d'environnement 'PORT' ou 'SERVER_PORT'
                // Sinon, écoute sur le port 8080 par défaut.
                .UseCloudHosting(9339)
                
                // Add VCAP_* configuration data
                .AddCloudFoundry()

                // Use Config Server for configuration data
                .AddConfigServer()

                // Ajouter le Steeltoe Dynamic Logging provider
                .AddDynamicLogging();
    }
}
